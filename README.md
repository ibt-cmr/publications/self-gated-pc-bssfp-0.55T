# Self Gated PC-bSSFP 0.55T

Reconstruction and analysis source code for "Self-Gated Cine Phase-Contrast bSSFP Flow Quantification at 0.55T".
Scripts are in the form of Jupyter Notebooks, with pre-saved outputs.


