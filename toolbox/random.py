
import numpy as np
import scipy.ndimage
import cv2
from scipy.optimize import leastsq
from matplotlib import pyplot as plt

def polyfit2d(image, kx=3, ky=3, order=None, mask=None):

    x = np.arange(0,image.shape[1])
    y = np.arange(0,image.shape[0])
    
    # grid coords
    x, y = np.meshgrid(x, y)
    # coefficient array, up to x^kx, y^ky
    coeffs = np.ones((kx+1, ky+1))
    
    if mask is not None:
        m = np.where(np.ravel(mask))
        d = np.ravel(image)[m]
    else:
        d = np.ravel(image)
        m = np.where(np.ones(d.shape))
    
    # solve array
    a = np.zeros((coeffs.size, d.size))
        

    # for each coefficient produce array x^i, y^j
    for index, (i, j) in enumerate(np.ndindex(coeffs.shape)):
        # do not include powers greater than order
        if order is not None and i + j > order:
            arr = np.zeros_like(x)
        else:
            arr = coeffs[i, j] * x**i * y**j
        a[index] = arr.ravel()[m]
        
    # do leastsq fitting and return leastsq result
    fit = np.linalg.lstsq(a.T, d, rcond=None)
    
    
    fitted_surf = np.polynomial.polynomial.polyval2d(x, y, fit[0].reshape((kx+1,ky+1)))
    return fitted_surf


def im_complex_median(image_series, filter_size_time, filter_size_space=1, filter_size_add_dim=1):
    
    
    kernel = (filter_size_space,filter_size_space,filter_size_time,) + (image_series.ndim-3)* (filter_size_add_dim,)
    
    fm = scipy.ndimage.median_filter(np.abs(image_series), size=kernel, mode='wrap')
    fc = scipy.ndimage.median_filter(np.angle(image_series), size=kernel, mode='wrap')
    
    return fm*np.exp(1j*fc)
    

def shift_radial_profiles(kspace, profiles, center_index):
    peaks = np.argmax(profiles,axis=0) - center_index
    return indep_roll(kspace,-peaks,axis=0)
    
def advanced_radial_shift(kspace,kx,ky, kz=None, factor=4, filter_sigma=1, fixed_xy_shift=False, plot=False):
    curves = np.sum(np.abs(kspace),axis=2)
    dsize=(curves.shape[1],curves.shape[0]*factor)
    
    resized_curves = cv2.resize(curves, dsize=dsize)
    
    filtered = scipy.ndimage.gaussian_filter1d(resized_curves,sigma=filter_sigma,axis=0)
    
    res_kx = cv2.resize(kx, dsize=dsize)
    res_ky = cv2.resize(ky, dsize=dsize)
    
    if kz is not None:
        res_kz = cv2.resize(kz, dsize=dsize)
    
    peaks = np.argmax(filtered,axis=0)
    
    if fixed_xy_shift and kz is None:
        # get angles of all spokes and sorting arrays
        angle_spoke = np.arctan2(kx[-1,:],ky[-1,:])
        sort = np.argsort(angle_spoke)
        unsort = np.argsort(sort)
        
        # sort peaks and angles
        d = peaks[sort]
        t =angle_spoke[sort]
        
        # setup inital values for fit
        init_mean = np.mean(d)
        init_std = -3*np.std(d)/2
        init_phase = 1
        
        # Perform fit on sin function
        optimize_func = lambda x: x[0]*np.sin(t+x[1])**2 + x[2] - d.astype(np.float32)
        est_amp, est_phase, est_mean = leastsq(optimize_func, [init_std ,init_phase, init_mean])[0]
        
        # calculate new peak locations for shift
        peak_fit = est_amp*np.sin(angle_spoke[sort]+est_phase)**2 + est_mean
        peaks = np.round(peak_fit).astype(np.int32)
        # invert sort
        peaks = peaks[unsort]
        
        if plot:
            fig, a = plt.subplots(1, 1, figsize=(8, 4))
            
            a.plot(d,'.',color='r')
            a.plot(peak_fit,'k')
            
            
            
    
    kx = kx - res_kx[peaks,np.arange(0,peaks.shape[0])]
    ky = ky - res_ky[peaks,np.arange(0,peaks.shape[0])]
    
    if kz is not None:
        kz = kz - res_kz[peaks,np.arange(0,peaks.shape[0])] 
        return (kx,ky,kz)   
    else:
        return (kx,ky)
    
# Taken from stack over flow, Yann Dubois, https://stackoverflow.com/questions/20360675/roll-rows-of-a-matrix-independently
def indep_roll(arr, shifts, axis=1):
    """Apply an independent roll for each dimensions of a single axis.

    Parameters
    ----------
    arr : np.ndarray
        Array of any shape.

    shifts : np.ndarray
        How many shifting to use for each dimension. Shape: `(arr.shape[axis],)`.

    axis : int
        Axis along which elements are shifted. 
    """
    arr = np.swapaxes(arr,axis,-1)
    all_idcs = np.ogrid[[slice(0,n) for n in arr.shape]]

    # Convert to a positive shift
    shifts[shifts < 0] += arr.shape[-1] 
    all_idcs[-1] = all_idcs[-1] - shifts[:, np.newaxis]

    result = arr[tuple(all_idcs)]
    arr = np.swapaxes(result,-1,axis)
    return arr
