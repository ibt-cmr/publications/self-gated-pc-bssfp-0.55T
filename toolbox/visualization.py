import numpy as np
from matplotlib import pyplot as plt
from matplotlib import colors as colors
from matplotlib import animation
from moviepy.editor import ImageSequenceClip
from PIL import Image
from copy import deepcopy

def save_to_video(filename,data, cmap_mag='gray', cmap_phase='bwr', imtype='both', magthresh=98, duration=1):
    
    a1 = 1
    a2 = 2
    if data.shape[3] > 2:
        a1 = 2
        a2 = 1
    
    if imtype=='both' or imtype=='mag':
        mag = np.transpose(np.abs(data),[3,2,0,1]) 
        mag = mag/np.percentile(mag,magthresh)
        mag[mag>1] = 1
        mag = np.concatenate(list(mag),axis=a1)
        
        mag_cm = plt.cm.get_cmap(cmap_mag)
        mag = mag_cm(mag)
        
    if imtype=='both' or imtype=='phase':
        phase = np.transpose(np.angle(data),[3,2,0,1]) 
        phase = np.mod(phase+np.pi,2*np.pi)/(2*np.pi)
        phase[phase>1] = 1
        phase = np.concatenate(list(phase),axis=a1)
        
        if cmap_phase == "bgr":
            segmentdata = {}
            grey = 0.4
            grey_width = 0.06
            segmentdata["green"] =[[0., 0., 0.],[0.5-grey_width/2, grey, grey],[0.5+grey_width/2, grey, grey],[1.,0.,0.]]
            segmentdata["red"] =[[0., 0., 0.],[0.5-grey_width/2, grey, grey],[0.5+grey_width/2, grey, 1.],[1.,1.,1.]]
            segmentdata["blue"] =[[0., 1., 1.],[0.5-grey_width/2, 1., grey],[0.5+grey_width/2, grey, grey],[1.,0.,0.]]
            phase_cm = colors.LinearSegmentedColormap('bgr',segmentdata)
        else:
            phase_cm = plt.cm.get_cmap(cmap_phase)
        phase = phase_cm(phase)
        
    
    if imtype=='both':      
        final = np.concatenate([mag,phase],axis=a2)           
    elif imtype=='mag':
        final = mag      
    elif imtype=='phase':
        final = phase
    
    

def save_to_gif(filename, data, cmap_mag='gray', cmap_phase='bwr', imtype='both', magthresh=98, duration=1):
    
    a1 = 1
    a2 = 2
    if data.shape[3] > 2:
        a1 = 2
        a2 = 1

    if imtype=='both' or imtype=='mag':
        mag = np.transpose(np.abs(data),[3,2,0,1]) 
        
        mthr = deepcopy(mag)
        mthr[np.isinf(mthr)]=0
        mthr[np.isnan(mthr)]=0
        thresh = np.percentile(mthr,magthresh)
        mag[np.isinf(mag)]=-1
        mag = mag/thresh
        mag[mag>1] = 1
        mag = np.concatenate(list(mag),axis=a1)
        
        mag_cm = plt.cm.get_cmap(cmap_mag)
        mag = mag_cm(mag)
        
    if imtype=='both' or imtype=='phase':
        phase = np.transpose(np.angle(data),[3,2,0,1]) 
        phase = np.mod(phase+np.pi,2*np.pi)/(2*np.pi)
        phase[phase>1] = 1
        phase = np.concatenate(list(phase),axis=a1)
        
        if cmap_phase == "bgr":
            segmentdata = {}
            grey = 0.4
            grey_width = 0.06
            segmentdata["green"] =[[0., 0., 0.],[0.5-grey_width/2, grey, grey],[0.5+grey_width/2, grey, grey],[1.,0.,0.]]
            segmentdata["red"] =[[0., 0., 0.],[0.5-grey_width/2, grey, grey],[0.5+grey_width/2, grey, 1.],[1.,1.,1.]]
            segmentdata["blue"] =[[0., 1., 1.],[0.5-grey_width/2, 1., grey],[0.5+grey_width/2, grey, grey],[1.,0.,0.]]
            phase_cm = colors.LinearSegmentedColormap('bgr',segmentdata)
        else:
            phase_cm = plt.cm.get_cmap(cmap_phase)
        phase = phase_cm(phase)
        
    
    if imtype=='both':      
        final = np.concatenate([mag,phase],axis=a2)           
    elif imtype=='mag':
        final = mag      
    elif imtype=='phase':
        final = phase
        
    # clip = ImageSequenceClip(list((255*final).astype(np.uint8)), fps=final.shape[0]/duration)
    # clip.write_gif(filename, fps=final.shape[0]/duration,logger=None)

    iml = []
    for im in list(final):
        im = Image.fromarray((255*im).astype(np.uint8))
        iml.append(im)

    iml[0].save(
            filename,
            save_all=True,
            append_images=iml[1:], # append rest of the images
            duration=np.round(duration/final.shape[0]*1000), # in milliseconds
            loop=0,
            optimize=False)
        
    return final


    
    
    
    
    
    
    
    
    
    
    
    
    
