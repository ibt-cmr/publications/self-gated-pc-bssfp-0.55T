import numpy as np
from matplotlib import pyplot as plt
from matplotlib import colors as colors



def blue_gray_red(cutoff_width = 0.05, grey_level = 0.5, bad_color = None):
    
    
    # Generate custom colormap

    segmentdata = {}
    grey = grey_level
    
    if cutoff_width is not None:
        grey_width = cutoff_width
        lim_col = 1.
    else:
        grey_width = 0
        lim_col = grey

    segmentdata["green"] =[[0., 0., 0.],
                           [0.5-grey_width/2, grey, grey],
                           [0.5+grey_width/2, grey, grey],
                           [1.,0.,0.]]

    segmentdata["red"] =[[0., 0., 0.],
                         [0.5-grey_width/2, grey, grey],
                         [0.5+grey_width/2, grey, lim_col],
                         [1.,1.,1.]]

    segmentdata["blue"] =[[0., 1., 1.],
                          [0.5-grey_width/2, lim_col, grey],
                          [0.5+grey_width/2, grey, grey],
                          [1.,0.,0.]]

    bgr = colors.LinearSegmentedColormap('bgr',segmentdata)

    if bad_color is not None:
        bgr.set_bad(bad_color,1.)

    return bgr