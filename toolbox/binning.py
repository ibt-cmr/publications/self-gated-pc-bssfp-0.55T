from copy import deepcopy
from scipy import signal
import numpy as np
from matplotlib import pyplot as plt

def get_k0(rawdata, kx, ky):
    
    k0_ind = np.argmin(kx**2+ky**2,axis=0)
    #k0_ind = np.argwhere((kx==0)*(ky==0))
    #return rawdata[np.arange(0,np.shape(k0_ind)[0]),k0_ind]
    return rawdata[k0_ind,np.arange(0,np.shape(k0_ind)[0])]

def find_best_coils(k0, TR, HRrange):
    
    Fs = 1/TR*1000
    freq = np.arange(0,k0.shape[0])*Fs/k0.shape[0]

    ind_start = int(np.round(HRrange[0]/60/(Fs/k0.shape[0])))
    ind_end = int(np.round(HRrange[1]/60/(Fs/k0.shape[0])))

    score = []

    for c in range(k0.shape[1]):

        ff = np.fft.fft(k0[:,c])

        sig_wind = np.abs(ff[ind_start:ind_end])
        score.append(np.max(sig_wind) - np.median(sig_wind))
    
    return score

def extract_filtered_peaks(k0, TR, f_lowpass=None, f_highpass=None, trim_thresh=None, flip_signal=True, ignore_start = None,reject_close_peaks=False, lowpass_order=4, highpass_order=5, expected_HR=None, plot=False):

    ksum = np.sum(np.abs(k0),1)
    Fs = 1/TR*1000

    ksum = ksum - np.mean(ksum)

    filt = signal.butter(lowpass_order,f_lowpass,'lowpass', fs=Fs)
    ksum = signal.lfilter(filt[0],filt[1],ksum)
    
    if f_highpass is not None:
        filt_high = signal.butter(highpass_order,f_highpass,'highpass', fs=Fs)
        ksum = signal.lfilter(filt_high[0],filt_high[1],ksum)
       
    if flip_signal:
        ksum = -ksum

    # Normalize for plotting
    center = np.percentile(ksum,50)
    sigma = np.percentile(ksum,90) - center
    ksum = (ksum-center)/(2*sigma)+0.5
    
    ksum_prethresh = deepcopy(ksum)
    
    if trim_thresh is None:
        trim_thresh = 0.5
    ksum[ksum < trim_thresh] = 0
        
    peaks = signal.find_peaks(ksum)[0]
    
    if ignore_start is not None:
        
        ignored = peaks[:ignore_start]
        peaks = peaks[ignore_start:]
        
    if reject_close_peaks:
        if expected_HR is not None:
            expected_p2p = 1/(expected_HR/60)*1000/TR
            peaks = near_peaks_rejection(peaks, expected_p2p = expected_p2p)
        else:
            peaks = near_peaks_rejection(peaks)
        
    
    if plot:
        
        f = Fs*np.arange(0,ksum.shape[0])/ksum.shape[0] - Fs/2;
        t = np.arange(0,ksum.shape[0])*TR/1000;
        kt = np.sum(np.abs(k0),1)
        
        fig, a = plt.subplots(4, 1, figsize=(12, 8))
        
        im = a[0].plot(f,np.fft.fftshift(np.log(np.abs(np.fft.fft(kt)))))
        if f_highpass is not None:
            im = a[0].plot(f,np.fft.fftshift(np.log(np.abs(np.fft.fft(signal.lfilter(filt_high[0],filt_high[1],signal.lfilter(filt[0],filt[1],kt-np.mean(kt))))))))
        else:
            im = a[0].plot(f,np.fft.fftshift(np.log(np.abs(np.fft.fft(signal.lfilter(filt[0],filt[1],kt-np.mean(kt)))))))
        a[0].set_xlim([-3,3])
        a[0].set_xlabel("Frequency, Hz")
        a[0].set_ylabel("Log |FFT K0|")
        a[0].set_title("FFT K0")
        
        im = a[1].plot(t,ksum_prethresh)
        a[1].set_ylim([-0.5,1.5])
        a[1].plot(t,ksum_prethresh*0+trim_thresh)
        a[1].set_xlabel("Time, s")
        a[1].set_ylabel("Relative K0 Signal")
        a[1].set_title("Filtered K0 Signal")

        im = a[2].plot(t,ksum)
        im = a[2].plot(t[peaks],ksum[peaks],'bv',ms=5)
        if ignore_start is not None and not ignore_start==0 :
            im = a[2].plot(t[ignored],ksum[ignored],'rv',ms=5)
        a[2].set_ylim([0,2])
        a[2].set_xlabel("Time, s")
        a[2].set_ylabel("Peak Detection")
        a[2].set_title("Peak Detection")
        
        d = np.diff(peaks)*TR/1000
        im = a[3].plot(peaks[:-1]*TR/1000, d)
        a[3].set_xlabel("Time, s")
        a[3].set_ylabel(r"$\Delta t$ Between Peaks, s")
        a[3].set_title("Peak Delta")
        fig.tight_layout()
        
        
    return peaks

def generate_bin_labels(peaks, phases, numTR, arrhythmia_rejection_factor=0.5):  

    p2p = np.diff(peaks)

    p2p_ref = np.median(p2p)
    
    # Get peak indices with too long rr interval
    arr = np.argwhere((p2p/p2p_ref)>(1+arrhythmia_rejection_factor))
    
    # set these to the reference rr interval
    p2p[arr] = np.round(p2p_ref)
    
    # Create array representing the indices of each bin relative to its peak
    phase_inds = np.round(np.outer(p2p,np.linspace(0,1,phases+1))).astype(np.int32)
    phase_inds = phase_inds[:,:-1]
    
    # Convert relatve to absolute indices of each bin
    abs_inds = phase_inds + np.transpose(peaks[:-1])[...,np.newaxis]
    abs_inds = abs_inds.flatten()
    
    # Get duration of every bin 
    phase_diff = np.diff(abs_inds)
    phase_diff = np.append(phase_diff,phase_diff[-1])
    
    # Repeat the repective phase label by the duration of each bin
    phase_label = np.tile(np.arange(0,phases),phase_inds.shape[0])
    phase_num = np.repeat(phase_label,phase_diff)
    
    # generate final label array
    final_label = -1*np.ones(numTR)
    final_label[peaks[0]:peaks[0]+phase_num.shape[0]] = phase_num
    
    # here we assume that in the case of long RR arrhythmia we actually just missed a peak, 
    # so we assume it would have a normal interval and set the missed interval to -1 (ignore)
    for ary in arr:
        final_label[(peaks[ary][0]+int(p2p_ref)):peaks[ary+1][0]] = -1
        
    # Alternative would be to use none of the data in the long RR interval, so we would set it all to -1 (ignore)
    # for ary in arr:
    #     final_label[peaks[ary][0]:peaks[ary+1][0]] = -1

    return final_label.astype(np.int32)


def near_peaks_rejection(peaks, expected_p2p=None):
    
    p2p = np.diff(peaks)
    if expected_p2p is None:
        ref_p2p = np.median(p2p)
    else:
        ref_p2p = expected_p2p
    
    peaks_filtered = []
    
    # assume first peak is good
    
    peaks_filtered.append(peaks[0])
    ind = 0
    
    prev_p2p = ref_p2p
    
    while peaks_filtered[-1] < peaks[-1]:
    
        last_good_peak = peaks_filtered[-1]
        expected_next_peak = last_good_peak + prev_p2p
        ind = np.argmin(np.abs(peaks[(ind+1):]-expected_next_peak)) + (ind+1)
        peaks_filtered.append(peaks[ind])
        
        new_peakdiff = peaks[ind] - last_good_peak
        if new_peakdiff > ref_p2p*1.4:
            prev_p2p = ref_p2p
        else:
            prev_p2p = new_peakdiff
    
    if (peaks_filtered[-1]-peaks_filtered[-2]) < ref_p2p*0.7 :
        del peaks_filtered[-1]
        
    return np.asarray(peaks_filtered)
        
    
    
    
    
    
    

def reshape_data_from_bins(kspace, kcoord, bin_labels, resp_bin_labels = None):
    
    k_list = []
    ksp_list = []
    label_list = []

    min_length = bin_labels.shape[0]
    card_phases = np.max(bin_labels)+1
    
    if resp_bin_labels is None:

        for cp in range(card_phases): 
            inds = np.argwhere(bin_labels==cp)

            k_list.append(kcoord[:,:,inds.flatten(),:])
            ksp_list.append(kspace[:,:,inds.flatten(),:])  

            min_length = np.minimum(min_length,ksp_list[cp].shape[2])

            label_list.append(bin_labels[inds.flatten()])  

        for cp in range(card_phases): 
            k_list[cp] = k_list[cp][:,:,:min_length,:]
            ksp_list[cp] = ksp_list[cp][:,:,:min_length,:]
            label_list[cp] = label_list[cp][:min_length]
            
        ksp_binned = np.transpose(np.asarray(ksp_list), axes=[1, 2, 3, 4, 0])
        k_binned = np.transpose(np.asarray(k_list), axes=[1, 2, 3, 4, 0])
        labels = np.asarray(label_list)  
    else:
        
        resp_phases = np.max(resp_bin_labels)+1
        
        for rp in range(resp_phases):
            
            k_list_sub = []
            ksp_list_sub = []
            label_list_sub = []
            
            for cp in range(card_phases): 
                inds = np.argwhere((bin_labels==cp) * (resp_bin_labels==rp))

                k_list_sub.append(kcoord[:,:,inds.flatten(),:])
                ksp_list_sub.append(kspace[:,:,inds.flatten(),:])  

                min_length = np.minimum(min_length,ksp_list_sub[cp].shape[2])

                label_list_sub.append(bin_labels[inds.flatten()])  
            
            k_list.append(k_list_sub)
            ksp_list.append(ksp_list_sub)
            label_list.append(label_list_sub)
        for rp in range(resp_phases):
            for cp in range(card_phases): 
                k_list[rp][cp] = k_list[rp][cp][:,:,:min_length,:]
                ksp_list[rp][cp] = ksp_list[rp][cp][:,:,:min_length,:]
                label_list[rp][cp] = label_list[rp][cp][:min_length]
                
        ksp_binned = np.transpose(np.asarray(ksp_list), axes=[2, 3, 4, 5, 1, 0])
        k_binned = np.transpose(np.asarray(k_list), axes=[2, 3, 4, 5, 1, 0])
        labels = np.asarray(label_list)  
    
    return ksp_binned, k_binned, labels

